from django.db import models


CAMA_LIBRE_OCUPADA = (
('O', 'Ocupada'),
('L', 'Libre'),
  )
CAMA_UTILIZABLE = (
('S', 'Se puede usar'),
('D', 'Des-habilitada'),
  )

class Edificio(models.Model):
    codigo = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=40)
    latitud = models.DecimalField(max_digits=15,decimal_places=5,default=0)
    longitud = models.DecimalField(max_digits=15,decimal_places=5,default=0)


class Sector(models.Model):
    edificio = models.ForeignKey(Edificio)
    codigo = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=40)

class Servicio(models.Model):
    sector = models.ForeignKey(Sector)
    codigo = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=40)

class Habitacion(models.Model):
    servicio = models.ForeignKey(Servicio)
    codigo = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=40)
    cantidadCamas = models.IntegerField()

class Equipo(models.Model):
    servicio = models.ForeignKey(Servicio)
    codigo = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=40)
    fabricante = models.CharField(max_length=40)
    numeroDeSerie = models.CharField(max_length=40)
    produccionDiaria = models.IntegerField()
    fabricante = models.CharField(max_length=40)

class Cama(models.Model):
    codigo = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=50)
    comodidades = models.CharField(max_length=50)
    habitacion = models.ForeignKey(Habitacion)
    utilizable = models.CharField(max_length=10, choices=CAMA_UTILIZABLE)
    ocupada = models.CharField(max_length=10, choices=CAMA_LIBRE_OCUPADA)

