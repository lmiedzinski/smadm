from django.contrib import admin
from contabilidad.models import TipoCuenta, Cuenta, TipoAsiento

admin.site.register([TipoCuenta, Cuenta, TipoAsiento])
