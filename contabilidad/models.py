from django.db import models


class TipoCuenta(models.Model):
    descripcion = models.CharField(max_length=40)

    def __unicode__(self):
        return self.descripcion


class Cuenta(models.Model):
    CUENTA_ESTADO_CHOICES = (
        (True, 'Alta'),
        (False, 'Baja'),
    )
    tipo = models.ForeignKey(TipoCuenta)
    descripcion = models.CharField(max_length=50)
    cuit = models.CharField(max_length=15, blank=True)
    estado = models.BooleanField(choices=CUENTA_ESTADO_CHOICES,
                              default=True)

    def __unicode__(self):
        return "[%s] %s" % (self.id, self.descripcion)


class TipoAsiento(models.Model):
    descripcion = models.CharField(max_length=40)
    proximoNumero = models.IntegerField()


class AsientoCabecera(models.Model):
    tipo = models.ForeignKey(TipoAsiento)
    numero = models.IntegerField()
    fecha = models.DateTimeField('fecha asiento')
    concepto = models.CharField(max_length=50)


class AsientoItem(models.Model):
    asiento = models.ForeignKey(AsientoCabecera)
    concepto = models.CharField(max_length=50)
    importe = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    imputacion = models.CharField(max_length=1)
    cuenta = models.ForeignKey(Cuenta)
