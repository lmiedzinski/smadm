"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""


from django.test import TestCase
from contabilidad.models import *


class TipoCuentasTestCase(TestCase):

    def setUp(self):
        "Se ejecuta antes de cada test"
        self.tipo_cuenta = TipoCuentas.objects.create(codigo="1000", descripcion="Foo")
        self.tipo_cuenta1 = TipoCuentas.objects.create(codigo="1001", descripcion="Foo1")
        tipo_cuenta2 = TipoCuentas.objects.create(codigo="1008", descripcion="Foo1")
        tipo_cuenta = TipoCuentas.objects.get(codigo="1008")
        self.assertEqual(tipo_cuenta,tipo_cuenta2 )        
        cuenta1 = Cuentas.objects.create(tipo=self.tipo_cuenta, codigo="1002", descripcion="bancos1002")
        cuenta2 = Cuentas.objects.create(tipo=self.tipo_cuenta1, codigo="1003", descripcion="bancos10033")
        cuenta3 = Cuentas.objects.create(tipo=tipo_cuenta2, codigo="1004", descripcion="bancos1004")
        cuenta = Cuentas.objects.get(codigo="1003")
        self.assertEqual(cuenta,cuenta2 )        

    def test_new_tipo_cuenta(self):
        "testea si self.tipocuenta se ha creado bien"
        tipo_cuenta = TipoCuentas.objects.get(codigo="1000")
        self.assertEqual(tipo_cuenta, self.tipo_cuenta)        
        tipo_cuenta = TipoCuentas.objects.get(codigo="1001")
        self.assertEqual(tipo_cuenta, self.tipo_cuenta1)        
        cuenta = Cuentas.objects.create(tipo=self.tipo_cuenta, codigo="1103", descripcion="bancos1002")
        cuenta2 = Cuentas.objects.get(codigo="1103")
        self.assertEqual(cuenta2, cuenta)        


class CuentasTestCase(TestCase):
    
    def setUp(self):
        "Se ejecuta antes de cada test"
        self.tipo_cuenta = TipoCuentas.objects.create(codigo="1000", descripcion="Test")
        self.cuenta = Cuentas.objects.create(tipo=self.tipo_cuenta, codigo="1001", descripcion="Caja")

    def test_new_cuenta(self):
        "testea si self.cuenta se ha creado bien"
        tipo = TipoCuentas.objects.get(codigo="1000")
        cuenta = Cuentas.objects.get(codigo="1001")
        self.assertEqual(cuenta, self.cuenta)
        self.assertEqual(cuenta.tipo, tipo)        

class CuentasTestCase1(TestCase):
    
    def setUp(self):
        "Se ejecuta antes de cada test"
        self.tipo_cuenta = TipoCuentas.objects.create(codigo="1000", descripcion="Test")
        self.cuenta = Cuentas.objects.create(tipo=self.tipo_cuenta, codigo="1001", descripcion="Caja")

    def test_new_cuenta(self):
        "testea si self.cuenta se ha creado bien"
        tipo = TipoCuentas.objects.get(codigo="1000")
        cuenta = Cuentas.objects.get(codigo="1001")
        self.assertEqual(cuenta, self.cuenta)
        self.assertEqual(cuenta.tipo, tipo)        
