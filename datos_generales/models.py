from django.db import models


class Nomenclador(models.Model):
    codigo = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=100)

class Cie10(models.Model):
    codigo = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=100)

