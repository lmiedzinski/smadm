
from django.db import models
from personal.models import Persona



SEXO_CHOICES = (
('M', 'Masculino'),
('F', 'Femenino'),
('I', 'Indefinido'),
  )
TIPO_DOC_CHOICES = (
('DNI', 'DNI'),
('CI', 'CI'),
('LE', 'LE'),
('SD', 'SinDatos'),
('LC', 'LC'),
('PA', 'Pasaporte'),
  )
class Financiador(models.Model):
    codigo = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=40)
    direccion = models.CharField(max_length=40)
    email = models.CharField(max_length=40)
    contacto = models.CharField(max_length=40)
    cuit = models.CharField(max_length=13)

class Provincia(models.Model):
    codigo = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=40)

class Localidad(models.Model):
    codigo = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=40)
    codigoPostal = models.CharField(max_length=10)
    provincia = models.ForeignKey(Provincia)


class Padron(models.Model):
    numeroAfiliado = models.CharField(max_length=20)
    nombre = models.CharField(max_length=40)
    financiador = models.ForeignKey(Financiador)
    tipoDocumento = models.CharField(max_length=5, choices=TIPO_DOC_CHOICES)
    numeroDocumento = models.CharField(max_length=8)
    fechaNacimiento=models.DateTimeField('fecha nacimiento')
    sexo = models.CharField(max_length=1, choices=SEXO_CHOICES)
    fechaIngresoAlSistema=models.DateTimeField('fecha ingreso')
    fechaEgresoDelSistema=models.DateTimeField('fecha egreso')

class Paciente(Persona):
    historiaClinica = models.CharField(max_length=10)

class PacienteFinanciador(models.Model):
    numeroAfiliado = models.CharField(max_length=20)
    financiador = models.ForeignKey(Financiador)
    paciente = models.ForeignKey(Paciente)
    fechaCarga=models.DateTimeField('fecha carga')

