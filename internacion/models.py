from django.db import models
from paciente.models import Paciente
from institucion.models import Cama,  Servicio
from datos_generales.models import Cie10
from personal.models import Medico

TIPO_INGRESO = (
('1', 'Guardia'),
('2', 'Ambulancia'),
('3', 'Desde Otra Institucion'),
('4', 'Programado'),
('5', 'Uso Quirofano'),
('6', 'Otros'),
  )
TIPO_EGRESO = (
('1', 'Alta medica definitiva'),
('2', 'Alta medica transitoria'),
('3', 'Traslado a otra institucion'),
('4', 'Fuga'),
('5', 'Retiro voluntario'),
('6', 'Obito'),
('7', 'Otros'),
  )


class Internado(models.Model):
    paciente = models.ForeignKey(Paciente)
    servicio = models.ForeignKey(Servicio)
    diagnosticoIngreso = models.ForeignKey(Cie10, null=True, related_name='diagnosticoIngreso')
    diagnosticoEgreso = models.ForeignKey(Cie10, null=True, related_name='diagnosticoEgreso')
    fechaIngreso = models.DateTimeField('fecha ingreso')
    fechaEgreso = models.DateTimeField('fecha egreso')
    tipoIngreso =  models.CharField(max_length=1, choices=TIPO_INGRESO)
    tipoEgreso =   models.CharField(max_length=1, choices=TIPO_EGRESO)
    medicoSolicitante = models.ForeignKey(Medico,null=True, related_name='solicitante')
    medicoTratante = models.ForeignKey(Medico, null=True, related_name='responsable')

class MovimientoCama(models.Model):
    internado = models.ForeignKey(Internado)
    cama = models.ForeignKey(Cama)
    fechaIngreso=models.DateTimeField('fecha ingreso')
    fechaEgreso=models.DateTimeField('fecha egreso')

