from django.db import models

class TipoMedicamento(models.Model):
    codigo = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=40)

class Sector(models.Model):
    codigo = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=40)

class Proveedor(models.Model):
    codigo = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=50)
    cuit = models.CharField(max_length=11)
    contacto = models.CharField(max_length=30)
    direccion = models.CharField(max_length=30)
    observaciones = models.CharField(max_length=50)
    
class Medicamento(models.Model):
    codigo = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=40)
    tipo = models.ForeignKey(TipoMedicamento)


class FacturaCabecera(models.Model):
    proveedor = models.ForeignKey(Proveedor)
    numero = models.CharField(max_length=14)
    fechaRecepcion = models.DateTimeField('fecha recepcion')
    fechaPago = models.DateTimeField('fecha pago')
    importeBruto = models.DecimalField(max_digits=15,decimal_places=2,default=0)
    importeImpuestos = models.DecimalField(max_digits=15,decimal_places=2,default=0)

class FacturaItem(models.Model):
    factura = models.ForeignKey(FacturaCabecera)
    medicamento = models.ForeignKey(Medicamento)
    lote = models.CharField(max_length=50)
    precioUnitario = models.DecimalField(max_digits=15,decimal_places=2,default=0)
    cantidad = models.DecimalField(max_digits=15,decimal_places=2,default=0)
    fechaVencimiento=models.DateTimeField('fecha vencimiento')

class Egreso(models.Model):
    medicamento = models.ForeignKey(Medicamento)
    sector = models.ForeignKey(Sector)
    cantidad = models.DecimalField(max_digits=15,decimal_places=2,default=0)
    fechaEgreso=models.DateTimeField('fecha egreso')
