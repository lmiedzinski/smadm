
from django.db import models


SEXO = (
('M', 'Masculino'),
('F', 'Femenino'),
('I', 'Indefinido'),
  )
TIPO_DOCUMENTO = (
('DNI', 'Documento Nacional de Identidad'),
('CI', 'Cedula De Identidad'),
('LC', ''),
('LE', 'Des-habilitada'),
  )

class Persona(models.Model):
    numeroDocumento = models.CharField(max_length=10)
    sexo = models.CharField(max_length=3, choices=TIPO_DOCUMENTO)
    apellidoNombre = models.CharField(max_length=50)
    direccion = models.CharField(max_length=50)
    localidad = models.CharField(max_length=50)
    telefono = models.CharField(max_length=30)
    codigoPostal = models.CharField(max_length=10)
    fechaNacimiento=models.DateTimeField('fecha nacimiento')
    sexo = models.CharField(max_length=1, choices=SEXO)


class Medico(Persona):
    matriculaNacional = models.CharField(max_length=10)
    matriculaProvincial = models.CharField(max_length=10)
    especialidad = models.CharField(max_length=40)


